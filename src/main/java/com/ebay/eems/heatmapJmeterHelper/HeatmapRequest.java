package com.ebay.eems.heatmapJmeterHelper;


import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

public class HeatmapRequest
{
    public final static String CURRENT_PROTOCOL_VERSION = "1.0";
    // Structure to drive GSON serialization of the commAttributes subsection
    protected class CommAttributes
    {
        @Expose
        protected String customerId = null;
        @Expose
        protected String messageId = null;
        @Expose
        protected String clientId = null;
        @Expose
        protected String cehId = null;
        @Expose
        protected String contactId = null;
        @Expose
        protected String campaignId = null;
        
        @Override
        public boolean equals(
                              Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            CommAttributes other = (CommAttributes) obj;
            return testEqual(customerId, other.customerId) && testEqual(messageId, other.messageId) && testEqual(clientId, other.clientId) &&
                    testEqual(cehId, other.cehId) && testEqual(contactId, other.contactId) && testEqual(campaignId, other.campaignId);
        }
    }

    // Structure to drive GSON serialization of the headers subsection
    protected class HeaderAttributes
    {
        @Expose
        protected String To;
        @Expose
        protected String From;
        @Expose
        public String Subject;
        
        @Override
        public boolean equals(
                              Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            HeaderAttributes other = (HeaderAttributes) obj;
            return testEqual(To, other.To) && testEqual(From, other.From) && testEqual(Subject, other.Subject);
        }
    }
    
    // Structure to drive GSON serialization of the email subsection
    protected class EmailAttributes
    {
        @Expose
        protected String trackingId;
        @Expose
        protected String plainText;
        @Expose
        protected String htmlText;
        @Expose
        protected HeaderAttributes headers = new HeaderAttributes();
        @Expose
        protected CommAttributes commAttributes = new CommAttributes();
        
        @Override
        public boolean equals(
                              Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            EmailAttributes other = (EmailAttributes) obj;
            return testEqual(trackingId, other.trackingId) && testEqual(plainText, other.plainText) && testEqual(htmlText, other.htmlText) &&
                    testEqual(headers, other.headers) && testEqual(commAttributes, other.commAttributes);
        }
    }
    
    // Structure to drive GSON serialization of the object subsection
    protected class ObjectAttributes
    {
        @Expose
        protected EmailAttributes email = new EmailAttributes();
        
        @Override
        public boolean equals(
                              Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            ObjectAttributes other = (ObjectAttributes) obj;
            return testEqual(email, other.email);
        }
    }
    
    public static class HeatmapUrl {
        // url in link
        @Expose
        public String url; 
        
        // text associated with link
        @Expose
        public String text;
        
        // location of link in the form of a polygon with five x,y, coordinates, e.g.
        //    717,101,717,346,751,346,751,101,717,101
        @Expose
        public String location;
        
        // messageUrl Id of link
        @Expose
        public String messageUrlId;
        
        public HeatmapUrl(String url, String text, String location, String messageUrlId)
        {
            this.url = url;
            this.text = text;
            this.location = location;
            this.messageUrlId = messageUrlId;
        }
        
        @Override
        public boolean equals(
                              Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            HeatmapUrl other = (HeatmapUrl) obj;
            return testEqual(url, other.url) && testEqual(text, other.text) &&
                    testEqual(location, other.location) && testEqual(messageUrlId, other.messageUrlId);
        }
    }
    
    @Expose
    protected ObjectAttributes object = new ObjectAttributes();
    
    @Expose
    protected String protocol_version = CURRENT_PROTOCOL_VERSION;
    
    @Expose
    protected String imageFile;
    
    // links in html
    @Expose
    private List<HeatmapUrl> urlList = new LinkedList<HeatmapUrl>();

    public String getClientId()
    {
        return object.email.commAttributes.clientId;
    }
    
    public void setClientId(
                       String clientId)
    {
        object.email.commAttributes.clientId = clientId;
    }
    
    public String getCehId()
    {
        return object.email.commAttributes.cehId;
    }
    
    public void setCehId(
                         String cehId)
    {
        object.email.commAttributes.cehId = cehId;
    }
    
    public String getContactId()
    {
        return object.email.commAttributes.contactId;
    }
    
    public void setContactId(
                             String contactId)
    {
        object.email.commAttributes.contactId = contactId;
    }
    
    public String getCampaignId()
    {
        return object.email.commAttributes.campaignId;
    }
    
    public void setCampaignId(
                              String campaignId)
    {
        object.email.commAttributes.campaignId = campaignId;
    }
    
    public String getCustomerId()
    {
        return object.email.commAttributes.customerId;
    }
    
    public void setCustomerId(
                              String customerId)
    {
        object.email.commAttributes.customerId = customerId;
    }
    
    public String getMessageId()
    {
        return object.email.commAttributes.messageId;
    }
    
    public void setMessageId(
                             String messageId)
    {
        object.email.commAttributes.messageId = messageId;
    }
    
    public String getTo()
    {
        return object.email.headers.To;
    }
    
    public void setTo(
                             String to)
    {
        object.email.headers.To = to;
    }
    
    public String getFrom()
    {
        return object.email.headers.From;
    }
    
    public void setFrom(
                             String from)
    {
        object.email.headers.From = from;
    }
    
    public String getSubject()
    {
        return object.email.headers.Subject;
    }
    
    public void setSubject(
                             String subject)
    {
        object.email.headers.Subject = subject;
    }
    
    public String getTrackingId()
    {
        return object.email.trackingId;
    }
    
    public void setTrackingId(
                             String trackingId)
    {
        object.email.trackingId = trackingId;
    }
    
    public String getPlainText()
    {
        return object.email.plainText;
    }
    
    public void setPlainText(
                             String plainText)
    {
        object.email.plainText = plainText;
    }
    
    public String getHtmlText()
    {
        return object.email.htmlText;
    }
    
    public void setHtmlText(
                             String htmlText)
    {
        object.email.htmlText = htmlText;
    }
    
    public String getProtocolVersion()
    {
        return protocol_version;
    }
    
    public void setProtocolVersion(
                             String protocol_version)
    {
        this.protocol_version = protocol_version;
    }
    
    public String getImageFile()
    {
        return imageFile;
    }
    
    public void setImageFile(
                             String imageFile)
    {
        this.imageFile = imageFile;
    }
    
    public void addUrl(HeatmapUrl url)
    {
        urlList.add(url);
    }
    
    public List<HeatmapUrl> getUrlList()
    {
        return urlList;
    }
    
    /**
     * Convert to json string
     * @return json string
     */
    public String toJson()
    {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    
    @Override
    public boolean equals(
                          Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        HeatmapRequest other = (HeatmapRequest) obj;
        if (!(testEqual(object, other.object) || !testEqual(protocol_version, other.protocol_version) || !testEqual(imageFile, other.imageFile))) return false;
        if (urlList.size() != other.urlList.size()) return false;
        for (int i = 0; i < urlList.size(); i++)
            if (!urlList.get(i).equals(other.urlList.get(i))) return false;
        return true;
    }
    
    private static boolean testEqual(Object o1, Object o2)
    {
        if (o1 == null)
        {
            if (o2 != null) return false;
        }
        else if (!o1.equals(o2)) return false;
        return true;
    }
}