package com.ebay.eems.heatmapJmeterHelper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.log.Logger;

import com.google.gson.Gson;

/**
 * Helper methods for InboxSimulator to call encoder and interpret results
 */
public class HeatmapJmeterHelper
{

    /**
     * Preprocessor used to set up JMS request. Should be called by BeanShell
     * PreProcessor
     * 
     * @param log
     * @throws NumberFormatException
     * @throws InterruptedException
     */
    public static void preprocessor(
                                    Logger log,
                                    String file)
            throws NumberFormatException,
            InterruptedException
    {
        JMeterContext context = JMeterContextService.getContext();
        JMeterVariables vars = context.getVariables();

        String messageId = UUID.randomUUID().toString();
        vars.put("messageId", messageId);

        Random rand = new Random();
        String customerId = "" + rand.nextInt(Integer.MAX_VALUE);
        vars.put("customerId", customerId);

        StringBuilder sb = new StringBuilder();
        // read in html file
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file));

            while (true)
            {
                String in = br.readLine();
                if (in == null) break;
                sb.append(in + "\n");
            }
            br.close();
        }
        catch (FileNotFoundException e)
        {
            log.error("File not found:" + file);
        }
        catch (IOException e)
        {
            log.error("IO exception reading " + file);
        }

        log.info("File " + file + " read, bytes=" + sb.length());
        HeatmapRequest hm = new HeatmapRequest();
        hm.setMessageId(messageId);
        hm.setCustomerId(customerId);

        hm.setTrackingId("3221");
        hm.setTo("joe@jack.com");
        hm.setFrom("jill@jane.com");
        hm.setSubject("Need water");
        hm.setPlainText("Hey Jack, the bucket is ...");
        hm.setCehId("34622");
        hm.setCampaignId("88439");
        hm.setContactId("32390");
        hm.setClientId("03003");

        hm.setHtmlText(sb.toString());
        String jsonText = null;
        jsonText = hm.toJson();
        vars.put("jmsText", jsonText);

    }

}
